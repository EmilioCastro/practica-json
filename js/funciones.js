$("document").ready(function(){
    var id = 1;
    $("#mostrar").click(function(){
        $.getJSON( "https://jsonplaceholder.typicode.com/photos/" + id, function( data ) {
            var items = [];
            $.each( data, function( key, val ) {
                if ( key == "albumId" ) {
                    console.log("AlbumID: " + val);  
                }
                else if ( key == "url" ) {
                    items.push( "<li id='" + key + "' class='list-group-item'><img src='" + val + "' class = 'img-fluid' /></li>" );
                }
                else if ( key == "thumbnailUrl" ) {
                    items.push( "<li id='" + key + "' class='list-group-item'><img src='" + val + "' class = 'img-fluid' /></li>" );
                }
                else {
                    items.push( "<li id='" + key + "' class='list-group-item'>" + val + "</li>" );
                };
           });
           
            $( "<ul/>", {
              "class": "list-group",
              html: items.join( "" )
            }).appendTo( "#lista" );
          });
        id += 1;
    });
});
# Práctica JSON #

Ésta práctica ejemplifica el uso de [JSON](http://www.json.org/) con [jQuery](https://jquery.com/).

### ¿Cómo funciona? ##

El script hace una petición a una URL que nos va a devolver un documento JSON, éste documento fue creado con una herramienta en línea que nos da datos de prueba en JSON llamada [JSON Placeholder](https://jsonplaceholder.typicode.com/).
Una vez que obtiene los datos, el script los interpreta y crea un elemento de lista que después es insertado en nuestro documento [HTML](https://www.w3schools.com/html/) para ser mostrado.

### ¿Preguntas? ###

Para eso está el [Slack](https://codellegeags.slack.com/)!!!